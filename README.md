BD Bot
======

[Bad Dragon](https://bad-dragon.com/) toy bot, available on [Telegram](https://telegram.org/) at [@BD_Toys](https://t.me/BD_Toys).
