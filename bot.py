#!/usr/bin/env python3

import argparse
import chevron
import requests
import time
import traceback
import pickle
import portalocker
from collections import namedtuple
from enum import Enum
from math import inf
from ruamel import yaml

templates = None
toyOptions = None
toyNames = None

class PublishedToy:
	def __init__(self, toy, lastSeen, messageId, messageText):
		self.toy = toy
		self.lastSeen = lastSeen
		self.messageId = messageId
		self.messageText = messageText

def bd_get_toy_page(page):
	print('Fetching page %d' % page)
	params = {
		'type[]': 'ready_made',
		'price[min]': '0',
		'price[max]': '300',
		'sort[field]': 'price',
		'sort[direction]': 'asc',
		'page': page,
		'limit': '60'
	}

	return requests.get('https://bad-dragon.com/api/inventory-toys', params=params).json()

def bd_get_toys():
	pageNum = 1
	toys = dict()

	while True:
		page = bd_get_toy_page(pageNum)
		if len(page['toys']) == 0:
			break
		for toy in page['toys']:
			# Ensure it is a number
			toy['id'] = int(toy['id'])
			toys[toy['id']] = toy
		pageNum += 1

	print('Fetched toys: %d' % len(toys))
	return toys

def bd_get_toy_names():
	toyById = dict()
	for toyModel in requests.get('https://bad-dragon.com/api/inventory-toys/product-list').json():
		toyById[toyModel['sku']] = toyModel['name']
	return toyById

toyNames = bd_get_toy_names()

def bd_get_toy_options():
	optionsByType = dict()
	for option in requests.get('https://bad-dragon.com/api/option-types-values').json():
		optionsByType[option['type']] = option
	return optionsByType

toyOptions = bd_get_toy_options()

def _bd_option_value_by_key(option, key, target):
	option = toyOptions.get(option)
	if not option:
		return None

	for value in option['values']:
		if value[key] == target:
			return value

	return None

def bd_option_value_by_id(option, value):
	return _bd_option_value_by_key(option, 'id', value)

def bd_option_value_by_value(option, value):
	return _bd_option_value_by_key(option, 'value', value)

def tg_req(botId, method, data):
	url = 'https://api.telegram.org/bot%s/%s' % (botId, method)
	req = requests.post(url, data=data)
	ret = req.json()

	# {'ok': False, 'error_code': 429, 'description': 'Too Many Requests: retry after 34', 'parameters': {'retry_after': 34}}
	if ret['ok'] == False and ret['error_code'] == 429:
		sleep_time = ret['parameters']['retry_after'] + 1
		print('Sleeping for %d seconds' % sleep_time)

		time.sleep(sleep_time)
		return tg_req(botId, method, data)

	return req.json()

def tg_send(botId, chatId, text):
	data = {
		'chat_id': chatId,
		'text': text,
		'parse_mode': 'Markdown',
		'disable_notification': 'true'
	}
	return tg_req(botId, 'sendMessage', data)

def tg_edit(botId, chatId, messageId, text):
	data = {
		'chat_id': chatId,
		'message_id': messageId,
		'text': text,
		'parse_mode': 'Markdown'
	}
	return tg_req(botId, 'editMessageText', data)

def tg_delete(botId, chatId, messageId):
	data = {
		'chat_id': chatId,
		'message_id': messageId
	}
	return bool(tg_req(botId, 'deleteMessage', data)['ok'])

def render_toy(toy):
	text = chevron.render(templates['toy'], {
		'id': toy['id'],
		'model': toyNames.get(toy['sku'], toy['sku']),
		'modelEmoji': templates['modelEmojis'].get(toy['sku'], ''),
		'link': 'https://bad-dragon.com/products/%s' % toy['sku'],
		'suctcup': toy['suction_cup'] == bd_option_value_by_value('suctionCup', '1')['id'],
		'cumtube': toy['cumtube'] == bd_option_value_by_value('cumtube', '1')['id'],
		'size': bd_option_value_by_id('size', toy['size'])['displayName'],
		'firmness': bd_option_value_by_id('firmness', toy['firmness'])['displayName'],
		'flop': toy['is_flop'],
		'flopreason': toy['external_flop_reason'],
		'price': '%.2f' % float(toy['price'])
	})

	if len(toy['images']) > 0:
		text = '[\u200B](%s)%s' % (toy['images'][0]['imageUrl1200'], text)
	return text

def save_published_toys(publishedToys, file):
	file.seek(0)
	file.truncate(0)
	pickle.dump(publishedToys, file)

parser = argparse.ArgumentParser(description='Updates McDonalds offers')
parser.add_argument('config', help='YAML configuration', type=argparse.FileType('r'))
args = parser.parse_args()

config = yaml.safe_load(args.config)
with open(config['templates']) as f:
	templates = yaml.safe_load(f)

try:
	dbHandle = open(config['publishedDb'], 'r+b')
	portalocker.lock(dbHandle, portalocker.LOCK_EX)
	publishedToys = pickle.load(dbHandle)
except FileNotFoundError:
	dbHandle = open(config['publishedDb'], 'w+b')
	portalocker.lock(dbHandle, portalocker.LOCK_EX)
	publishedToys = dict()
	save_published_toys(publishedToys, dbHandle)

now = time.time()
currentToys = bd_get_toys()

for toy in currentToys.values():
	publishedToy = publishedToys.get(toy['id'])
	toyText = render_toy(toy)
	if publishedToy:
		if toyText != publishedToy.messageText:
			print('%d: updating' % toy['id'])

			try:
				message = tg_edit(config['botId'], config['channelId'], publishedToy.messageId, toyText)
				if message['ok']:
					publishedToy.toy = toy
					publishedToy.messageText = toyText
					save_published_toys(publishedToys, dbHandle)

			except Exception as e:
				traceback.print_last()

		publishedToy.lastSeen = now
	else:
		print('%s: new toy' % toy['id'])

		try:
			message = tg_send(config['botId'], config['channelId'], toyText)
			if message['ok']:
				publishedToys[toy['id']] = PublishedToy(toy, now, message['result']['message_id'], toyText)
				save_published_toys(publishedToys, dbHandle)
			else:
				print('Update failed: ' + repr(message))
		except Exception as e:
			traceback.print_last()

for unavailable in publishedToys.keys() - currentToys.keys():
	publishedToy = publishedToys[unavailable]
	if now - publishedToy.lastSeen >= config['maxAge']:
		print('%d: unavailable' % publishedToy.toy['id'])

		try:
			if not tg_delete(config['botId'], config['channelId'], publishedToy.messageId):
				print('FUCK, failed to delete message %d' % publishedToy.messageId)
				tg_edit(config['botId'], config['channelId'], publishedToy.messageId, templates['deleted'])
			del publishedToys[unavailable]
			save_published_toys(publishedToys, dbHandle)
		except Exception as e:
			traceback.print_last()

dbHandle.close()
